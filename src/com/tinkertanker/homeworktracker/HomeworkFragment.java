package com.tinkertanker.homeworktracker;

import java.io.File;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;

public class HomeworkFragment extends Fragment {
	
	public static final String EXTRA_HOMEWORK_ID = "com.tinkertanker.homeworktracker.homework_id";
	
	private Homework mHomework;
	private EditText mTitleField;
	private CheckBox mFinishedCheckbox;
	private DatePicker mDatePicker;
	private ImageButton mCameraButton;
	private String mPhotoName;
	private Button mSMSButton;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override 
	public View onCreateView(LayoutInflater inflater, 
			ViewGroup parent, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_homework, parent, false);
		
		Activity a = getActivity();
		UUID homeworkID = (UUID)a.getIntent().getSerializableExtra(EXTRA_HOMEWORK_ID);
		if (homeworkID != null) { // called as an activity, so ID was in the extra
			mHomework = HomeworkCollection.get(a).getHomework(homeworkID);
		} else { // called as a fragment detail, check the arguments bundle
			homeworkID = (UUID)getArguments().getSerializable(EXTRA_HOMEWORK_ID);
			mHomework = HomeworkCollection.get(getActivity()).getHomework(homeworkID);			
		}

		mTitleField = (EditText)view.findViewById(R.id.homework_title);
		mTitleField.setText(mHomework.getTitle());

		mFinishedCheckbox = (CheckBox)view.findViewById(R.id.homework_checkbox);
		mFinishedCheckbox.setChecked(mHomework.isFinished());

		mDatePicker = (DatePicker)view.findViewById(R.id.homework_date_picker);
		
		mTitleField.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				mHomework.setTitle(s.toString());
				Log.d("Homework", "Set homework title to " + mHomework.getTitle());
			}
		});
		
		Calendar cal = Calendar.getInstance();
		mDatePicker.init(cal.get(Calendar.YEAR), 
				cal.get(Calendar.MONTH),
				cal.get(Calendar.DAY_OF_MONTH), 
				new DatePicker.OnDateChangedListener() {

			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				Date doneDate = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
				mHomework.setDate(doneDate);
				Log.d("Homework", "Set homework date to " + mHomework.getDate());
			}
		});
		
		mFinishedCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				mHomework.setFinished(arg1);
				Log.d("Homework", "Set homework finished to " + mHomework.isFinished());
			}
		});		
		
		mCameraButton = (ImageButton)view.findViewById(R.id.camera_button);
		mCameraButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// Use an implicit intent from MediaStore
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                
                // set up the path name and create a file to save the image
                File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                mPhotoName = DateFormat.getDateTimeInstance().format(new Date()) + ".jpg";
                File output = new File(dir, mPhotoName);
                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));

                // start the image capture Intent
                startActivityForResult(i, 0);
                				
			}
		});
		
		if (mHomework.getPhotoName() != null) {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            File output = new File(dir, mHomework.getPhotoName());
    		Uri uri = Uri.fromFile(output);
			mCameraButton.setImageURI(uri);
		}
		
		mSMSButton = (Button)view.findViewById(R.id.send_sms_button);
		mSMSButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {

//				Intent sms = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:"));
//				sms.putExtra("sms_body", mHomework.getTitle());
//				startActivity(sms);
				
//				Intent i = new Intent(Intent.ACTION_SEND);
//				i.setType("message/rfc822");
//				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"recipient@example.com"});
//				i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
//				i.putExtra(Intent.EXTRA_TEXT   , "body of email");
//				try {
//				    startActivity(Intent.createChooser(i, "Send mail..."));
//				} catch (android.content.ActivityNotFoundException ex) {
//				    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
//				}
//				
				Intent bt = new Intent(Intent.ACTION_SEND);
				bt.setType("image/jpg");
	            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
	            File output = new File(dir, mHomework.getPhotoName());
	    		Uri uri = Uri.fromFile(output);
				bt.putExtra(Intent.EXTRA_STREAM, uri);
				
				// Get a list of apps that can handle our intent
				PackageManager pm = getActivity().getPackageManager();
				List<ResolveInfo> appsList = pm.queryIntentActivities(bt, 0);
				
				// Look through this list for the exact Bluetooth handler
				String packageName = null;
				String className = null;
				boolean found = false;
				for (ResolveInfo info: appsList) {
					packageName = info.activityInfo.packageName;
					if( packageName.endsWith("bluetooth")) {
						className = info.activityInfo.name;
						found = true;
						break;// found, can exit
					}
				}
				
				// If it's found, launch our activity
				if (found) {
					bt.setClassName(packageName, className);
					startActivity(bt);
				} else {
					// perhaps a toast here to say 'no bluetooth'?
				}

				
			}
		});
		
		return view;
	}
	
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// Check the request is for taking a photo and OK
    	if (resultCode == Activity.RESULT_OK) {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            File output = new File(dir, mPhotoName);
    		Uri uri = Uri.fromFile(output);
    		mCameraButton.setImageURI(uri);
    		mHomework.setPhotoName(mPhotoName);
    	}
    }

	
	@Override
	public void onPause() {
		super.onPause();
		Log.d("Homework", "Saving homework!");
		try {
			HomeworkCollection.get(getActivity()).saveHomework();
		} catch (Exception e) {
			Log.e("Homework", "Error saving homework: ", e);
		}
	}
	
}
