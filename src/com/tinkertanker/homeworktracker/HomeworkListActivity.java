package com.tinkertanker.homeworktracker;

import java.util.UUID;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class HomeworkListActivity extends FragmentActivity
	implements HomeworkListFragment.Callbacks
{

	public void onHomeworkSelected(Homework h) {

		UUID id = h.getId();
		
		if (findViewById(R.id.tablet_detail_fragment) == null) { 
			// no detail fragment found, so we set up an activity for phone
			Intent i = new Intent(this, HomeworkActivity.class);
			i.putExtra(HomeworkFragment.EXTRA_HOMEWORK_ID, id);
			startActivity(i);
		} else { // there was a detail fragment, so this is for the tablet
			HomeworkFragment fragment = new HomeworkFragment();
			Bundle arguments = new Bundle();
			arguments.putSerializable(HomeworkFragment.EXTRA_HOMEWORK_ID, id);
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.tablet_detail_fragment, fragment).commit();			
		}
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_masterdetail);
		
	}
}
