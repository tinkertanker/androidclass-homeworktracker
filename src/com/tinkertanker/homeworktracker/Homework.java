package com.tinkertanker.homeworktracker;

import java.util.Date;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

public class Homework {
	
	private UUID mId;
	private String mTitle;
	private Date mDate;
	private boolean mFinished;
	private String mPhotoName;
	
	public Homework() {
		mId = UUID.randomUUID();
		mDate = new Date();
	}
	
	public Homework(JSONObject json) throws JSONException {
		mId = UUID.fromString(json.getString("id"));
		mTitle = json.getString("title");
		mFinished = json.getBoolean("finished");
		mDate = new Date(json.getLong("date"));
		mPhotoName = json.getString("photoName");
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("id", mId.toString());
		json.put("title", mTitle);
		json.put("date", mDate.getTime());
		json.put("finished", mFinished);
		json.put("photoName", mPhotoName);
		return json;
	}


	public Date getDate() {
		return mDate;
	}

	public void setDate(Date date) {
		mDate = date;
	}

	public boolean isFinished() {
		return mFinished;
	}

	public void setFinished(boolean finished) {
		mFinished = finished;
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		mTitle = title;
	}

	public UUID getId() {
		return mId;
	}
	
	public String getPhotoName() {
		return mPhotoName;
	}
	
	public void setPhotoName(String photoName) {
		mPhotoName = photoName;
	}
	
	@Override
	public String toString() {
		return mTitle;
	}
	

}
