package com.tinkertanker.homeworktracker;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

public class HomeworkListFragment extends ListFragment {
	private ArrayList<Homework> mHomeworkList;
	
	private Callbacks mCallbacks;
	
	public interface Callbacks {
		void onHomeworkSelected(Homework h);
	}

	@Override 
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mCallbacks = (Callbacks)activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActivity().setTitle(R.string.homework_list_title);
		HomeworkCollection hc = HomeworkCollection.get(getActivity());
		mHomeworkList = hc.getHomeworkList();
		
		HomeworkAdapter adapter = new HomeworkAdapter(mHomeworkList);
		setListAdapter(adapter);
		setHasOptionsMenu(true);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// Get the homework from the adapter
		Homework h = ((HomeworkAdapter)getListAdapter()).getItem(position);
		// Start the HomeworkActivity
		mCallbacks.onHomeworkSelected(h);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		((HomeworkAdapter)getListAdapter()).notifyDataSetChanged();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		((HomeworkAdapter)getListAdapter()).notifyDataSetChanged();
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_homework_list, menu);
	}
	
	@Override 
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.menu_item_new_homework:
			Homework newHomework = new Homework();
			HomeworkCollection hc = HomeworkCollection.get(getActivity());
			hc.addHomework(newHomework);
			mCallbacks.onHomeworkSelected(newHomework);
//			Intent i = new Intent(getActivity(), HomeworkActivity.class);
//			i.putExtra(HomeworkFragment.EXTRA_HOMEWORK_ID, newHomework.getId());
//			startActivityForResult(i, 0);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private class HomeworkAdapter extends ArrayAdapter<Homework> {
		public HomeworkAdapter(ArrayList<Homework> homeworkList) {
			super(getActivity(), 0, homeworkList);
		}
		
		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				view = getActivity().getLayoutInflater().inflate(R.layout.list_item_homework, null);
			}
			Homework h = getItem(position);
			
			TextView titleTextView = (TextView)view.findViewById(R.id.homework_list_item_titleTextView);
			titleTextView.setText(h.getTitle());
			
			TextView dateTextView = (TextView)view.findViewById(R.id.homework_list_item_dateTextView);
			dateTextView.setText(h.getDate().toString());
			
			CheckBox finishedCheckBox = (CheckBox)view.findViewById(R.id.homework_list_item_doneCheckBox);
			finishedCheckBox.setChecked(h.isFinished());
			
			return view;
		}
		
	}
	
}
