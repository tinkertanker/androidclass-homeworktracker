package com.tinkertanker.homeworktracker;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class HomeworkActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homework);
	}

}
