package com.tinkertanker.homeworktracker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.content.Context;
import android.util.Log;

public class HomeworkCollection {
	private static HomeworkCollection sHomeworkCollection;
	private Context mAppContext;
	private ArrayList<Homework> mHomeworkList;
	
	private HomeworkCollection(Context appContext) {
		mAppContext = appContext;
		try {
			loadHomework();
		} catch (Exception e) {
			mHomeworkList = new ArrayList<Homework>();
			Log.e("Homework", "Error loading homework: ", e);
		}
	}
	
	public static HomeworkCollection get(Context c) {
		if (sHomeworkCollection == null) {
			sHomeworkCollection = new HomeworkCollection(c.getApplicationContext());
		}
		return sHomeworkCollection;
	}
	
	public ArrayList<Homework> getHomeworkList() {
		return mHomeworkList;
	}
	
	public void addHomework(Homework h) {
		mHomeworkList.add(h);
	}
	
	public Homework getHomework(UUID id) {
		for (Homework h : mHomeworkList) {
			if (h.getId().equals(id)) {
				return h;
			}
		}
		return null;
	}
	
	public void saveHomework() throws JSONException, IOException {
		// Build the array in JSON
		JSONArray array = new JSONArray();
		for (Homework h : mHomeworkList) {
			array.put(h.toJSON());
		}
		Writer writer = null;
		try {
			OutputStream out = mAppContext.openFileOutput("savefile.json", Context.MODE_PRIVATE);
			writer = new OutputStreamWriter(out);
			writer.write(array.toString());
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}
	
	public void loadHomework() throws JSONException, IOException {
		mHomeworkList = new ArrayList<Homework>();
		BufferedReader reader = null;
		try {
			// Open and read file
			InputStream in = mAppContext.openFileInput("savefile.json");
			reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder jsonString = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				jsonString.append(line);
			}
			// Parse
			JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
			for (int i = 0; i < array.length(); i++) {
				mHomeworkList.add(new Homework(array.getJSONObject(i)));
			}
		} catch (FileNotFoundException e) {
			// ignore
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}
	
}
